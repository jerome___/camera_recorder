#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    camera = NULL;
    imageCapture = NULL;
    mediaRecorder = NULL;
    setVideo();
}

MainWindow::~MainWindow() {
    delete ui;
    if (camera)
        camera = NULL;
    delete dp;
}

void MainWindow::on_actionQuitter_triggered() {
//	users->disconnectUser();
    this->close();
}

void MainWindow::setVideo() {
    ui->captureImage->setEnabled(false);
    itemOverlayText = new QGraphicsTextItem;
    // recVideoWIdget = new QVideoWidget;
    recVideoItem = new QGraphicsVideoItem;
    recVideoItem->setSize(QSize(640, 480));
    recVideoItem->setAspectRatioMode(Qt::KeepAspectRatioByExpanding);
    recVideoItem->setOpacity(1.0);
    scene = new QGraphicsScene(this);
    scene->addItem(recVideoItem);
    //scene->addWidget(recVideoWidget);
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    ui->videoSourceComboBox->addItem(QString("aucune"));
    foreach (const QCameraInfo &cameraInfo, cameras)
        ui->videoSourceComboBox->addItem(cameraInfo.description());
}

void MainWindow::on_videoSourceComboBox_currentIndexChanged(const QString &arg1) {
    if (ui->videoSourceComboBox->currentIndex() != 0) {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            if(cameraInfo.description() == arg1) {
                qDebug() << "video device choosed is:" << cameraInfo.deviceName();
                ui->video->setScene(scene);
                camera = new QCamera(cameraInfo);
                camera->setViewfinder(recVideoItem);
                camera->setCaptureMode(QCamera::CaptureVideo);
                connect(camera, SIGNAL(error(QCamera::Error)), this, SLOT(displayCameraError()));
                imageCapture = new QCameraImageCapture(camera);
                mediaRecorder = new QMediaRecorder(camera);
                connect(mediaRecorder, SIGNAL(durationChanged(qint64)),
                        this, SLOT(updateRecordTime()));
                connect(mediaRecorder, SIGNAL(error(QMediaRecorder::Error)),
                        this, SLOT(displayRecorderError()));
                mediaRecorder->setMetaData(QMediaMetaData::Title,
                                           QVariant(QLatin1String("Test Title")));
                connect(imageCapture, SIGNAL(readyForCaptureChanged(bool)),
                        this, SLOT(readyForCapture(bool)));
                connect(imageCapture, SIGNAL(imageSaved(int,QString)),
                        this, SLOT(imageSaved(int,QString)));
                connect(imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), this,
                        SLOT(displayCaptureError(int,QCameraImageCapture::Error,QString)));
                foreach(QString meta, camera->availableMetaData())
                    qDebug() << "camera meta datas are:" << meta;
                itemOverlayText->setPlainText(tr("%1 sélectionnée").arg(arg1));
                if (ui->overlay->isChecked()) scene->addItem(itemOverlayText);
                ui->video->show();
                camera->start(); } } }
    else {
        QGraphicsScene *textNULL = new QGraphicsScene;
        textNULL->addText("aucune source d'entrée");
        ui->video->setScene(textNULL);
        if (camera)
            camera->deleteLater(); }
}

void MainWindow::on_stop_clicked() {
    ui->record->setChecked(false);
    ui->play->setChecked(false);
    ui->pause->setChecked(false);
    mediaRecorder->stop();
}

void MainWindow::updateRecordTime() {
    QString str = QString("Recorded %1 sec").arg(mediaRecorder->duration()/1000);
    ui->statusBar->showMessage(str);
}

void MainWindow::on_chooseDirRec_clicked() {
    dp = new QFileDialog(this,tr("Choose a directory for record files from source."),
                         QDir::homePath());
    connect(dp, SIGNAL(directoryEntered(QString)), this, SLOT(limitToCampaignDir(QString)));
    dp->setOption(QFileDialog::ShowDirsOnly);
    dp->setFileMode(QFileDialog::Directory);
    int answer = dp->exec();
    if (answer == 1)
        ui->dirRec->setText(dp->directory().absolutePath());
}

void MainWindow::limitToCampaignDir(QString dir) {
    QRegExp rgx(QDir::homePath());
    if (rgx.indexIn(dir) == -1)
        dp->setDirectory(QDir::homePath());
}

void MainWindow::on_outputFileEdit_textChanged(const QString &arg1) {
    dirToRec = ui->dirRec->text() + "/" + arg1;
}

void MainWindow::displayCameraError() {
    QMessageBox::warning(this, tr("Camera error"), camera->errorString());
}

void MainWindow::displayRecorderError() {
    QMessageBox::warning(this, tr("Capture error"), mediaRecorder->errorString());
    ui->pause->setChecked(false);
}
void MainWindow::on_overlay_toggled(bool checked) {
    checked ? scene->addItem(itemOverlayText) : scene->removeItem(itemOverlayText);
}

void MainWindow::readyForCapture(bool ready) {
    ui->captureImage->setEnabled(ready);
}

void MainWindow::on_captureImage_clicked() {
    camera->setCaptureMode(QCamera::CaptureStillImage);
    imageCapture->capture();
    camera->setCaptureMode(QCamera::CaptureVideo);
}

void MainWindow::on_record_toggled(bool checked) {
    if (!dirToRec.isEmpty()) {
        mediaRecorder->setOutputLocation(QUrl::fromLocalFile(dirToRec));
        if (checked)
            mediaRecorder->record();
        else {
            mediaRecorder->stop();
            ui->statusBar->clearMessage(); }
        updateRecordTime(); }
    else {
        ui->record->setChecked(false);
        QMessageBox::warning(this, tr("choose a directory and a file name "
                                      "for record source video capture first.")); }

}

void MainWindow::on_pause_toggled(bool checked) {
    mediaRecorder->pause();
}
