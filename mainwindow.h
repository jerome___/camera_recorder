#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>
#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
//#include <QCameraViewfinder>
#include <QGraphicsVideoItem>
#include <QtMultimedia>
#include <QDir>
#include <QFileDialog>

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_videoSourceComboBox_currentIndexChanged(const QString &arg1);
    void displayCameraError();
    void on_actionQuitter_triggered();
    void on_chooseDirRec_clicked();
    void limitToCampaignDir(QString dir);
    void on_outputFileEdit_textChanged(const QString &arg1);
    void on_stop_clicked();
    void readyForCapture(bool ready);
    void updateRecordTime();
    void displayRecorderError();
    void on_overlay_toggled(bool checked);
    void on_captureImage_clicked();
    void on_record_toggled(bool checked);

    void on_pause_toggled(bool checked);

private:
    Ui::MainWindow      *ui;
    QCamera				*camera;
    QCameraImageCapture *imageCapture;
    QGraphicsVideoItem  *recVideoItem;
    QGraphicsScene      *scene;
    QGraphicsTextItem   *itemOverlayText;
    QMediaRecorder      *mediaRecorder;
    QFileDialog         *dp;
    QString              dirToRec;
    void setVideo();
};

#endif // MAINWINDOW_H
