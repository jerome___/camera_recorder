# README #

test case application with Qt-5.7 to try QCamera video source and QGrapgicsView container

### What is this repository for? ###

* Camera_Record would like to become a little test case Qt application for try to use and give an example of QCamera with QGraphicsView and overlay text on scene.


### How to try it ? ###

* I use it from Qt-5.7, maybe it should be use with other versions of Qt (tell me)

* you just have to clone this project and add it to your projects for compil and run it

* Be free to do what ever you want with it, and be free to show me what should be a best way to use QGraphicsView with QCamera by add fonctionnality if you want to share knowledge to.

### Current problems ###

* Actually, i see that, from archlinux OS, with Xfce-4, if i change virtual desktop, then the image from camera source freeze.

* the same code use in a more big programm failed: it set camera ON, then show a black-screen one second, and stop to show screen source (camera stay ON), the overlay text works fine.