#-------------------------------------------------
#
# Project created by QtCreator 2016-07-30T15:46:13
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets xml xmlpatterns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Camera_Recorder
TEMPLATE = app
CONFIG += c++14


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    images/images.qrc
